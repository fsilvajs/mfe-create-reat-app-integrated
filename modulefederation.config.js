const { dependencies } = require('./package.json');

module.exports = {
  remotes: {
    remote: 'remote@http://localhost:3000/remoteEntry.js',
  },
  shared: {
    ...dependencies,
    react: {
      singleton: true,
      requiredVersion: dependencies['react'],
    },
    'react-dom': {
      singleton: true,
      requiredVersion: dependencies['react-dom'],
    },
  },
};
