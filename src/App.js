import { lazy, Suspense } from 'react';
import './App.css';

const RemoteComponent = lazy(() => import('remote/Button'));

function App() {
  return (
    <div className="App">
        <Suspense fallback="Loading Button">
          <RemoteComponent />
        </Suspense>
    </div>
  );
}

export default App;
